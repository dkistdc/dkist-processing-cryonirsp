Overview
========

The mission of the Cryogenic Near-IR Spectropolarimeter (Cryo-NIRSP) is to measure the full state of
polarization of spectral lines at near- and thermal-infrared wavelengths.  It measures the relatively
faint infrared corona and heliosphere, as well as the thermal infrared solar spectrum on disk. Cryo-NIRSP
has two arms: one scanning slit spectrograph and one imager; both can operate in a polarimetric mode.

The `dkist-processing-cryonirsp` code repository contains the implementation of the Cryo-NIRSP
calibration pipelines, which convert Level 0 files from the telescope into Level 1 output products.
Pipelines are built on `dkist-processing-common <https://docs.dkist.nso.edu/projects/common/>`_
tasks and use the `dkist-processing-core <https://docs.dkist.nso.edu/projects/core/>`_ framework.
Follow links on this page for more information about calibration pipelines.

Level 1 data products are available at the `DKIST Data Portal <https://dkist.data.nso.edu/>`_ and can
be analyzed with `DKIST python user tools <https://docs.dkist.nso.edu/projects/python-tools/>`_.  For
help, please contact the `DKIST Help Desk <https://nso.atlassian.net/servicedesk/customer/portals/>`_.
