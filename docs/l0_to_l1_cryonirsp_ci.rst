l0_to_l1_cryonirsp_ci
=====================

In the normal Cryo-NIRSP Context Imager (CI) operating mode, raw Cryo-NIRSP data are gathered at the summit and
delivered to the Data Center. The Data Center then calibrates this data and prepares it for storage using the
following workflow.

For more detail on each workflow task, you can click on the task in the diagram.

.. workflow_diagram:: dkist_processing_cryonirsp.workflows.ci_l0_processing.l0_pipeline
