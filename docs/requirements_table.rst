Package Requirements
====================

This table shows the main Python packages and their respective
versions that were used to build the calibration codes documented
here. All of these packages are available on `PyPI <https://pypi.org>`_.

.. requirements_table:: dkist-processing-cryonirsp
