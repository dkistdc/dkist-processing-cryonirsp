.. include:: landing_page.rst

.. toctree::
    :maxdepth: 2
    :hidden:

    self
    l0_to_l1_cryonirsp_ci
    l0_to_l1_cryonirsp_sp
    l0_to_l1_cryonirsp_ci-full-trial
    l0_to_l1_cryonirsp_sp-full-trial
    scientific_changelog
    linearization
    bad_pixel_calibration
    beam_boundary_computation
    beam_angle_calculation
    ci_science_calibration
    sp_science_calibration
    autoapi/index
    requirements_table
    changelog
