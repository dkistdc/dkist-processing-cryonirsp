v1.0.0 (2024-08-21)
===================

Misc
----

- CRYO-NIRSP processing pipeline data accepted for release to the community.


v0.0.1 (2023-08-25)
===================

Misc
----

- Initial release of pipeline for science review
